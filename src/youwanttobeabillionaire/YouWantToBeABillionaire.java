
package youwanttobeabillionaire;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author bruno
 */
public class YouWantToBeABillionaire extends Application {
    
    @Override
    public void start(Stage stage) throws IOException  {               
        Parent root = FXMLLoader.load(getClass().getResource("One_LandingPage.fxml"));
        
        Scene scene = new Scene(root);
        stage.setResizable(false);
        scene.getStylesheets().add("https://fonts.googleapis.com/css?family=Sacramento");
        scene.getStylesheets().add("https://fonts.googleapis.com/css?family=Nova+Oval");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
